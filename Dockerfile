FROM microsoft/dotnet:2.2-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /src
COPY ["dockerApplication/dockerApplication.csproj", "dockerApplication/"]
RUN dotnet restore "dockerApplication/dockerApplication.csproj"
COPY . .
WORKDIR "/src/dockerApplication"
RUN dotnet build "dockerApplication.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "dockerApplication.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "dockerApplication.dll"]